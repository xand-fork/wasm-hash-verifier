use serde::{Deserialize, Deserializer, Serialize, Serializer};

/// Wrapper type around blake3 hash
#[derive(Clone, Debug, PartialEq, Eq, Hash)]
#[allow(clippy::module_name_repetitions)]
pub struct V1XandHash(blake3::Hash);

impl V1XandHash {
    /// We should keep this struct `pub(crate)` to keep implementation details like our
    /// choice of hashing algorithm internal.
    pub(crate) const fn new(inner: blake3::Hash) -> Self {
        Self(inner)
    }
}

// `blake3::Hash` isn't `Serialize` or `Deserialize` so this is the intermediary type for
// manual implementations on `V1XandHash`
#[derive(Deserialize, Serialize)]
struct SerDeV1XandHash([u8; blake3::OUT_LEN]);

impl From<V1XandHash> for SerDeV1XandHash {
    fn from(xand_hash: V1XandHash) -> Self {
        Self(*xand_hash.0.as_bytes())
    }
}

impl From<SerDeV1XandHash> for V1XandHash {
    fn from(serde_hash: SerDeV1XandHash) -> Self {
        Self(serde_hash.0.into())
    }
}

impl Serialize for V1XandHash {
    fn serialize<S>(&self, serializer: S) -> Result<<S as Serializer>::Ok, <S as Serializer>::Error>
    where
        S: Serializer,
    {
        let serializable: SerDeV1XandHash = self.clone().into();
        serializable.serialize(serializer)
    }
}

impl<'de> Deserialize<'de> for V1XandHash {
    fn deserialize<D>(deserializer: D) -> Result<Self, <D as Deserializer<'de>>::Error>
    where
        D: Deserializer<'de>,
    {
        let serde_hash: SerDeV1XandHash = Deserialize::deserialize(deserializer)?;
        Ok(serde_hash.into())
    }
}
