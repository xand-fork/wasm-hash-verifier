use crate::xand_hash::v1::V1XandHash;
use alloc::vec::Vec;
use postcard::{from_bytes, to_allocvec};
use serde::{Deserialize, Deserializer, Serialize, Serializer};

pub mod v1;

/// Standard target type for `WasmBlob` hashing.
/// Versioning allows future hashed types to be added while not breaking existing code.
#[non_exhaustive]
#[derive(Clone, Debug, Eq, Hash, PartialEq)]
pub enum XandHash {
    V1(V1XandHash),
}

// **WARNING: Don't make public** There is panicking code in the `From` impls for `XandHash` that
// operate under the assumption that `SerdeXandHash` can only be constructed via `From`.
#[derive(Serialize, Deserialize)]
struct SerdeXandHash {
    variant: u8,
    inner: Vec<u8>,
}

#[allow(clippy::fallible_impl_from)]
impl From<SerdeXandHash> for XandHash {
    fn from(serde_xand_hash: SerdeXandHash) -> Self {
        match serde_xand_hash.variant {
            1 => {
                let contents = from_bytes(&serde_xand_hash.inner).expect("Should never fail: see prop tests");
                Self::V1(contents)
            }
            _ => unimplemented!("Invalid SerdeXandHash: no variant {:?}", serde_xand_hash.variant),
        }
    }
}

#[allow(unreachable_patterns, clippy::fallible_impl_from)]
impl From<&XandHash> for SerdeXandHash {
    fn from(xand_hash: &XandHash) -> Self {
        match xand_hash {
            XandHash::V1(contents) => {
                let variant = 1;
                let inner = to_allocvec(&contents).expect("Should never fail: see prop tests");
                Self { variant, inner }
            }
            _ => unreachable!("Add new variants here as needed"),
        }
    }
}

impl Serialize for XandHash {
    fn serialize<S>(&self, serializer: S) -> Result<<S as Serializer>::Ok, <S as Serializer>::Error>
    where
        S: Serializer,
    {
        let serde_xand_hash: SerdeXandHash = self.into();
        serde_xand_hash.serialize(serializer)
    }
}

impl<'de> Deserialize<'de> for XandHash {
    fn deserialize<D>(deserializer: D) -> Result<Self, <D as Deserializer<'de>>::Error>
    where
        D: Deserializer<'de>,
    {
        let serde_hash: SerdeXandHash = Deserialize::deserialize(deserializer)?;
        Ok(serde_hash.into())
    }
}

#[cfg(test)]
impl XandHash {
    #[must_use]
    pub const fn new(inner: blake3::Hash) -> Self {
        Self::V1(V1XandHash::new(inner))
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use proptest::prelude::*;

    prop_compose! {
        fn arbitrary_xand_hash()(
            bytes: [u8;32]
        ) -> XandHash {
            let blake3_hash = bytes.into();
            XandHash::new(blake3_hash)
        }
    }

    proptest! {
        #[test]
        fn v1__round_trip_serialization_deserialization(
            expected in arbitrary_xand_hash()
        ) {
            let serialized = bincode::serialize(&expected).unwrap();
            let actual: XandHash = bincode::deserialize(&serialized).unwrap();
            assert_eq!(expected, actual);
        }
    }
}
