use crate::xand_hash::v1::V1XandHash;
use crate::{hasher::WasmHasher, wasm_blob::WasmBlob, xand_hash::XandHash, WasmHashVerifier};

#[allow(clippy::module_name_repetitions)]
pub struct XandWasmHasher;

impl WasmHasher for XandWasmHasher {
    type Hash = XandHash;

    fn hash<T: AsRef<[u8]>>(blob: &WasmBlob<T>) -> Self::Hash {
        XandHash::V1(V1XandHash::new(blake3::hash(blob.as_ref())))
    }
}

impl WasmHashVerifier for XandWasmHasher {}
