use crate::wasm_blob::WasmBlob;

/// Interface for `WasmBlob` hasher implementations
#[allow(clippy::module_name_repetitions)]
pub trait WasmHasher {
    type Hash;

    fn hash<T: AsRef<[u8]>>(blob: &WasmBlob<T>) -> Self::Hash;
}
