use crate::{hasher::WasmHasher, wasm_blob::WasmBlob};

/// Interface for verifying hashes of `WasmBlobs`
#[allow(clippy::module_name_repetitions)]
pub trait WasmHashVerifier: WasmHasher
where
    Self::Hash: Eq,
{
    fn verify<T: AsRef<[u8]>>(hash: &Self::Hash, blob: &WasmBlob<T>) -> bool {
        &Self::hash(blob) == hash
    }
}
