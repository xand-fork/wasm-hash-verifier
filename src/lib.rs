#![cfg_attr(not(test), no_std)]
#![warn(
    clippy::all,
    clippy::nursery,
    clippy::pedantic,
    clippy::unreadable_literal,
    rust_2018_idioms
)]
// Safety-critical application lints
#![deny(clippy::pedantic, clippy::float_cmp_const, clippy::unwrap_used)]
#![allow(
    clippy::implicit_return,
    clippy::iter_nth_zero,
    clippy::match_bool,
    clippy::missing_errors_doc,
    clippy::non_ascii_literal,
    clippy::wildcard_imports,
    incomplete_features
)]
// To use the `unsafe` keyword, do not remove the `unsafe_code` attribute entirely.
// Instead, change it to `#![allow(unsafe_code)]` or preferably `#![deny(unsafe_code)]` + opt-in
// with local `#[allow(unsafe_code)]`'s on a case-by-case basis, if practical.
#![forbid(unsafe_code, bare_trait_objects)]
// Uncomment before ship to reconcile use of possibly redundant crates, debug remnants, missing
// license files and more
// #![allow(clippy::blanket_clippy_restriction_lints)]
// #![warn(clippy::cargo, clippy::restriction, missing_docs, warnings)]
// #![allow(clippy::implicit_return)]
#![cfg_attr(test, allow(non_snake_case, clippy::unwrap_used))]

extern crate alloc;

mod hasher;
mod verifier;
mod wasm_blob;
mod xand_hash;
mod xand_wasm_hasher;

pub use hasher::WasmHasher;
pub use verifier::WasmHashVerifier;
pub use wasm_blob::WasmBlob;
pub use xand_hash::XandHash;
pub use xand_wasm_hasher::XandWasmHasher;
