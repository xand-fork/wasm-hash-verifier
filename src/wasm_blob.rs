use serde::{Deserialize, Serialize};

/// Wrapper type for raw WASM bytes
#[derive(Clone, Debug, Deserialize, Eq, Hash, PartialEq, Serialize)]
pub struct WasmBlob<T: AsRef<[u8]>>(T);

impl<T: AsRef<[u8]>> AsRef<[u8]> for WasmBlob<T> {
    fn as_ref(&self) -> &[u8] {
        self.0.as_ref()
    }
}

impl<T> From<T> for WasmBlob<T>
where
    T: AsRef<[u8]>,
{
    fn from(value: T) -> Self {
        Self(value)
    }
}
