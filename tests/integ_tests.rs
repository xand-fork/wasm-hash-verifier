use core::convert::AsRef;
use wasm_hash_verifier::{WasmBlob, WasmHashVerifier, WasmHasher, XandHash, XandWasmHasher};

#[test]
fn wasm_blob_interface_check() {
    let values = vec![1, 2, 3, 4];
    let wasm_blob: WasmBlob<&[u8]> = WasmBlob::from(values.as_ref());
    let hash: XandHash = XandWasmHasher::hash(&wasm_blob);
    assert!(XandWasmHasher::verify(&hash, &wasm_blob));
}
